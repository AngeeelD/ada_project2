package angeeeld.utils;

import javafx.util.Pair;
import rx.Observable;
import rx.observables.StringObservable;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by angeeeld on 9/03/17.
 */
public class Utilities {

    public static HashMap<Integer, Point> readPointsFromRouteFile(File fileToRead) throws FileNotFoundException {
        HashMap<Integer, Point> points = new HashMap<>();
        return Observable.combineLatest(
                Observable.just(points),
                StringObservable.split(
                        StringObservable.from(new BufferedReader(new FileReader(fileToRead))), "\n")
                        .map(Utilities::parsePointLine)
                        .filter(Objects::nonNull)
                        .asObservable(),
                (integerPointHashMap, integerPointPair) -> {
                    integerPointHashMap.put(integerPointPair.getKey(), integerPointPair.getValue());
                    return integerPointHashMap;
                })
                .last().toSingle().toBlocking().value();
    }

    private static Pair<Integer, Point> parsePointLine(String line) {
        String[] lineData = line.split(" ");
        try {
            return new Pair<>(Integer.parseInt(lineData[0]),
                    new Point(Integer.parseInt(lineData[1]),
                            Integer.parseInt(lineData[2])));
        } catch (NumberFormatException | NullPointerException e) {
            return null;
        }
    }

    public static Double[][] getDistancesFromPoints(HashMap<Integer, Point> pointRelation) {

        Double[][] distancesBetweenPoints = new Double[pointRelation.size()][pointRelation.size()];

        List<Integer> cities = new ArrayList<>();
        cities.addAll(pointRelation.keySet());

        for (int i = 0; i < cities.size(); i++) {
            for (int j = 0; j < cities.size(); j++) {
                distancesBetweenPoints[i][j] = Math.hypot(
                        pointRelation.get(cities.get(j)).getX() - pointRelation.get(cities.get(i)).getX(),
                        pointRelation.get(cities.get(j)).getY() - pointRelation.get(cities.get(i)).getY());
            }
        }


        return distancesBetweenPoints;
    }

}
