package angeeeld.MainApplication;

import java.io.File;

/**
 * Created by angeeeld on 8/03/17.
 */
public interface MainApplicationContract {

    interface View {

    }

    interface Presenter {

        void findTheShortestRoute(File dataFile);

    }

}
