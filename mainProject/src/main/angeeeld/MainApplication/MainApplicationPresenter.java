package angeeeld.MainApplication;

import angeeeld.utils.Utilities;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;

/**
 * Created by angeeeld on 8/03/17.
 */
public class MainApplicationPresenter implements MainApplicationContract.Presenter {

    //region Global Variables
    private MainApplicationContract.View mView;
    //endregion

    //region Constructor
    public MainApplicationPresenter(MainApplicationContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void findTheShortestRoute(File dataFile) {
        try {
            HashMap<Integer, Point> pointRelation = Utilities.readPointsFromRouteFile(dataFile);
            Double[][] distances = Utilities.getDistancesFromPoints(pointRelation);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    //endregion

}
