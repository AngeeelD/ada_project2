package angeeeld.MainApplication;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by angeeeld on 8/03/17.
 */
public class MainApplication extends Application implements Initializable, MainApplicationContract.View {

    //region Global Variables
    @FXML
    private Button mAnalyzeButton;
    @FXML
    private ChoiceBox<KeyValuePair> mFilesToAnalyzeComboBox;
    @FXML
    private ProgressBar mProgressBar;

    MainApplicationContract.Presenter mPresenter;
    //endregion

    //region Initializer
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(ClassLoader.getSystemClassLoader().getResource("MainApplication.fxml"));

        Scene scene = new Scene(root);

        primaryStage.setTitle("ADA Project 2");
        primaryStage.setScene(scene);
        primaryStage.setAlwaysOnTop(true);
        primaryStage.show();
    }

    //endregion

    //region Local Methods
    private void initViews() {
        KeyValuePair defaultSelection = null;
        try {
            defaultSelection = new KeyValuePair(new Pair<>(
                    new File(ClassLoader.getSystemClassLoader().getResource("kroa100").toURI()), "Kroa100"));
            mFilesToAnalyzeComboBox.getItems().add(defaultSelection);
            mFilesToAnalyzeComboBox.getItems().add(new KeyValuePair(new Pair<>(
                    new File(ClassLoader.getSystemClassLoader().getResource("kroa150").toURI()), "Kroa150")));
            mFilesToAnalyzeComboBox.getItems().add(new KeyValuePair(new Pair<>(
                    new File(ClassLoader.getSystemClassLoader().getResource("kroa200").toURI()), "Kroa200")));
            mFilesToAnalyzeComboBox.setValue(defaultSelection);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void findTheShortestRoute(File fileToAnalyze) {
        mPresenter.findTheShortestRoute(fileToAnalyze);
    }
    //endregion

    //region View Controller
    @FXML
    protected void onAnalyzeClicked(ActionEvent event) {
        Platform.runLater(() -> findTheShortestRoute(mFilesToAnalyzeComboBox.getValue().getValue().getKey()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initViews();
        this.mPresenter = new MainApplicationPresenter(this);
    }
    //endregion

    //region Classes
    private class KeyValuePair {

        Pair<File, String> value;

        public KeyValuePair(Pair<File, String> value) {
            this.value = value;
        }

        public Pair<File, String> getValue() {
            return value;
        }

        public String toString() {
            return value.getValue();
        }

    }
    //endregion
}
